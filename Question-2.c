#include <stdio.h>
int main() {
  int x, i, flag=0;
  printf("Enter a Positive Integer: ");
  scanf("%d", &x);

  for (i=2; i<=x/2; ++i) {
    if (x%i==0) {
      flag=1;
      break;
    }
  }
  if (x==1) {
    printf("1 is neither prime nor composite.");
  }
  else {
    if (flag==0)
      printf("%d is a Prime Number.", x);
    else
      printf("%d is not a Prime Number.", x);
  }
  return 0;
}
