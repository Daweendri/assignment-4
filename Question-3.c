#include<stdio.h>
int main(){
    int x, reverse=0, rem;
    printf("Enter a Number: ");
    scanf("%d", &x);
    while(x!=0)
    {
      rem=x%10;
      reverse=reverse*10+rem;
      x/=10;
    }
    printf("Reversed Number: %d", reverse);
    return 0;
}
